# Design Museum Gent plugin for Craft CMS 3.x

DMG functionalities

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /design-museum-gent

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Design Museum Gent.

## Design Museum Gent Overview

-Insert text here-

## Configuring Design Museum Gent

-Insert text here-

## Using Design Museum Gent

-Insert text here-

## Design Museum Gent Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [The Craft](https://www.the-craft.be/)

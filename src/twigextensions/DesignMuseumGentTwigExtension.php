<?php
/**
 * Design Museum Gent plugin for Craft CMS 3.x
 *
 * DMG functionalities
 *
 * @link      https://www.the-craft.be/
 * @copyright Copyright (c) 2018 The Craft
 */

namespace thecraft\designmuseumgent\twigextensions;

use thecraft\designmuseumgent\DesignMuseumGent;

use Craft;

/**
 * @author    The Craft
 * @package   DesignMuseumGent
 * @since     0.0.1
 */
class DesignMuseumGentTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'DesignMuseumGent';
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('someFilter', [$this, 'someInternalFunction']),
            new \Twig_SimpleFilter('autoLink', [$this, 'autoLink'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('someFunction', [$this, 'someInternalFunction']),
        ];
    }

    /**
     * @param null $text
     *
     * @return string
     */
    public function someInternalFunction($text = null)
    {
        $result = $text . " in the way";

        return $result;
    }

    /**
     * @param null $text
     *
     * @return string
     */
    public function autoLink($text = null)
    {
        $regex = '/(\S+@\S+\.\S+)/';
        $replace = '<a href="mailto:$1">$1</a>';
        return preg_replace($regex, $replace, $text);
    }
}

<?php
/**
 * Design Museum Gent plugin for Craft CMS 3.x
 *
 * DMG functionalities
 *
 * @link      the-craft.be
 * @copyright Copyright (c) 2018 The Craft
 */

namespace thecraft\designmuseumgent\controllers;

use thecraft\designmuseumgent\DesignMuseumGent;;

use Craft;
use craft\web\Controller;

/**
 * @author    The Craft
 * @package   Test
 * @since     0.1
 */
class SproutController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the SproutController actionIndex() method';

        return $result;
    }

    /**
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the SproutController actionDoSomething() method';

        return $result;
    }
}

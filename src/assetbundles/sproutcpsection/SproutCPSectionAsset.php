<?php
/**
 * test plugin for Craft CMS 3.x
 *
 * test
 *
 * @link      the-craft.be
 * @copyright Copyright (c) 2018 The Craft
 */

namespace \test\assetbundles\sproutcpsection;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    The Craft
 * @package   Test
 * @since     0.1
 */
class SproutCPSectionAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@/test/assetbundles/sproutcpsection/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/Sprout.js',
        ];

        $this->css = [
            'css/Sprout.css',
        ];

        parent::init();
    }
}

<?php
/**
 * Design Museum Gent plugin for Craft CMS 3.x
 *
 * DMG functionalities
 *
 * @link      https://www.the-craft.be/
 * @copyright Copyright (c) 2018 The Craft
 */

namespace thecraft\designmuseumgent\services;

use thecraft\designmuseumgent\DesignMuseumGent;

use Craft;
use craft\base\Component;
use craft\elements\Entry;

/**
 * @author    The Craft
 * @package   DesignMuseumGent
 * @since     0.0.1
 */
class Agenda extends Component
{
    public function getYears($categories)
    {
        $currentYear = date('Y');

        $query = Entry::find()
            ->section('agenda')
            ->dateStart('< ' . date('Y-m-d'))
            ->orderBy('dateStart asc');
        
        if (count($categories)) {
            $query = $query->relatedTo($categories);
        }

        $query = $query->one();

        $earliestYear = $query->dateStart->format('Y');

        $yearStart = round($earliestYear - 5, -1);
        $yearEnd = round($currentYear - 5, -1);

        $data = [];

        for($year = $yearStart; $year <= $yearEnd; $year+=10) {
            $end = $year + 9;
            if($end > $currentYear) $end = $currentYear;
            $data[$end]['end'] = $end;
            $data[$end]['start'] = $year;
        }

        krsort($data);

        return $data;
    }
}

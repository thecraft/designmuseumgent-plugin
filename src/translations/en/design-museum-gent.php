<?php
/**
 * Design Museum Gent plugin for Craft CMS 3.x
 *
 * DMG functionalities
 *
 * @link      https://www.the-craft.be/
 * @copyright Copyright (c) 2018 The Craft
 */

/**
 * @author    The Craft
 * @package   DesignMuseumGent
 * @since     0.0.1
 */
return [
    'Design Museum Gent plugin loaded' => 'Design Museum Gent plugin loaded',
];

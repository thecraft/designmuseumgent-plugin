<?php
/**
 * Design Museum Gent plugin for Craft CMS 3.x
 *
 * DMG functionalities
 *
 * @link      https://www.the-craft.be/
 * @copyright Copyright (c) 2018 The Craft
 */

namespace thecraft\designmuseumgent\variables;

use thecraft\designmuseumgent\DesignMuseumGent;

use Craft;

/**
 * @author    The Craft
 * @package   DesignMuseumGent
 * @since     0.0.1
 */
class DesignMuseumGentVariable
{
    public function getYears($categories)
    {
        $years = DesignMuseumGent::getInstance()->agenda->getYears($categories);
        return $years;
    }
}

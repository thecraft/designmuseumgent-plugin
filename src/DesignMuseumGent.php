<?php
/**
 * Design Museum Gent plugin for Craft CMS 3.x
 *
 * DMG functionalities
 *
 * @link      https://www.the-craft.be/
 * @copyright Copyright (c) 2018 The Craft
 */

namespace thecraft\designmuseumgent;

use thecraft\designmuseumgent\services\Agenda as AgendaService;
use thecraft\designmuseumgent\variables\DesignMuseumGentVariable;
use thecraft\designmuseumgent\twigextensions\DesignMuseumGentTwigExtension;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\twig\variables\CraftVariable;

use yii\base\Event;

/**
 * Class DesignMuseumGent
 *
 * @author    The Craft
 * @package   DesignMuseumGent
 * @since     0.0.1
 *
 * @property  AgendaService $agenda
 */
class DesignMuseumGent extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var DesignMuseumGent
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '0.0.2';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Craft::$app->view->registerTwigExtension(new DesignMuseumGentTwigExtension());

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'test/sprout';
            }
        );

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['cpActionTrigger1'] = 'test/sprout/do-something';
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('designMuseumGent', DesignMuseumGentVariable::class);
            }
        );

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'design-museum-gent',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
